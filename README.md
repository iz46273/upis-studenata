# Upis studenata


## Table of Contents
* [Description](https://gitlab.com/iz46273/upis-studenata#description)
* [Technologies](https://gitlab.com/iz46273/upis-studenata#technologies)


### Description:
#### User management
* 2 roles: admin and student
* User login and registration

#### Student view
* Student can mark subjects as unenrolled, enrolled and passed.
* Student can see all the subjects' details.

#### Admin view
* Admin can see and update all subjects, delete or add a new one.
* Admin can see all students and their lists.
* Admin can change student's list.

### Technologies:
Python Django, SQL Database
