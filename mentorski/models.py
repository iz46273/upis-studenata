from django.db import models
from django.utils import timezone
from enum import Enum
from django.contrib.auth.models import AbstractUser

# Create your models here.

class Korisnici(AbstractUser):
    MENTOR = 'mentor'
    STUDENT = 'student'
    ROLE_CHOICES = (
        (MENTOR, 'Mentor'),
        (STUDENT, 'Student'),
    )
    NONE = 'none'
    REDOVNI = 'redovni'
    IZVANREDNI = 'izvanredni'
    STATUS_CHOICES = (
        (NONE, 'None'),
        (REDOVNI, 'Redovni'),
        (IZVANREDNI, 'Izvanredni'),
    )
    email = models.EmailField(('email adress'), unique=True)
    role = models.CharField(choices=ROLE_CHOICES, null=False, max_length=25)
    status = models.CharField(choices=STATUS_CHOICES, null=False, max_length=25)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.username

class Predmeti(models.Model):
    DA = 'da'
    NE = 'ne'
    IZBORNI_CHOICES = (
        (DA, 'Da'),
        (NE, 'Ne'),
    )
    ime = models.CharField(max_length=255, null=False)
    kod = models.CharField(max_length=16, null=False)
    program = models.TextField(null=False)
    bodovi = models.IntegerField(null=False)
    sem_redovni = models.IntegerField(null=False)
    sem_izvanredni = models.IntegerField(null=False)
    izborni = models.CharField(choices=IZBORNI_CHOICES, null=False, max_length=10)

class Upisi(models.Model):
    student = models.ForeignKey(Korisnici, on_delete=models.CASCADE, null=False)
    predmet = models.ForeignKey(Predmeti, on_delete=models.CASCADE, null=False)
    status = models.CharField(max_length=64, null=False)