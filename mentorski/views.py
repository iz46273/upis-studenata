from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import Korisnici, Upisi, Predmeti
from .forms import RegistrationForm, CourseForm, EditStudentForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user
from django.contrib.auth.models import User

# Create your views here.

def register(request):
    if request.method == 'GET':
        userForm = RegistrationForm()
        return render(request, 'register.html', {'form':userForm})
    elif request.method == 'POST':
        userForm = RegistrationForm(request.POST)
        if userForm.is_valid():
            userForm.save()
            return redirect('login')
        else:
            return render(request, 'register.html', {'form':userForm})
    else:
        return HttpResonseNotAllowed()

def index(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'student':
            student_id = current_user.id
            return redirect('details', student_id)
        else:
            return redirect('students')
    else:
        return redirect('login')

def students(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            all_users = Korisnici.objects.all()
            student_list = []
            for userr in all_users:
                if userr.role == 'student':
                    student_list.append(userr)
            return render(request, 'students.html', {'student_list':student_list})
        else:
            return redirect('index')
    else:
        return redirect('login')

def details(request, student_id):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'student':
            student_id = current_user.id
        all_upisi = Upisi.objects.all()
        all_courses = Predmeti.objects.all()
        if request.method == 'GET':
            student = Korisnici.objects.get(id=student_id)
            signed_courses = {}
            unsigned_courses = {}
            ects_passed = 0
            ects_enrolled = 0
            for upis in all_upisi:
                if upis.student == student:
                    if upis.status == 'enrolled':
                        signed_courses[upis.predmet] = [upis.predmet.id, upis.predmet.ime, upis.predmet.sem_redovni, upis.predmet.sem_izvanredni, upis.status]
                        ects_enrolled += upis.predmet.bodovi
                    elif upis.status == 'passed':
                        signed_courses[upis.predmet] = [upis.predmet.id, upis.predmet.ime, upis.predmet.sem_redovni, upis.predmet.sem_izvanredni, upis.status]
                        ects_passed += upis.predmet.bodovi
                    else:
                        unsigned_courses[upis.predmet] = [upis.predmet.id, upis.predmet.ime, upis.predmet.bodovi, upis.predmet.sem_redovni, upis.predmet.sem_izvanredni]
            for course in all_courses:
                provjera = 1
                for upis in all_upisi:
                    if upis.student == student:
                        if upis.predmet == course:
                            provjera = 0
                if provjera:
                    unsigned_courses[course] = [course.id, course.ime, course.bodovi, course.sem_redovni, course.sem_izvanredni]
            semestri_red = [1,2,3,4,5,6]
            semestri_izv = [1,2,3,4,5,6,7,8]
            context = {
                'signed_courses':signed_courses,
                'current_student':student,
                'all_courses':all_courses,
                'all_upisi':all_upisi,
                'unsigned_courses':unsigned_courses,
                'semestri_red':semestri_red,
                'semestri_izv':semestri_izv,
                'ects_passed':ects_passed,
                'ects_enrolled':ects_enrolled
            }
            return render(request, 'details.html', context)
    else:
        return redirect('login')

def upis(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            student_id = request.GET.get('student_id', '')
            course_id = request.GET.get('course_id', '')
            student = Korisnici.objects.get(id=student_id)
            course = Predmeti.objects.get(id=course_id)
            all_upisi = Upisi.objects.all()
            for upis in all_upisi:
                if upis.student == student and upis.predmet == course:
                    upis = Upisi.objects.get(id=upis.id)
                    upis.status = 'enrolled'
                    upis.save()
                    return redirect('details', student_id)
            upis = Upisi(student=student, predmet=course, status='enrolled')
            upis.save()
            return redirect('details', student_id)
    else:
        return redirect('login')

def ispis(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            student_id = request.GET.get('student_id', '')
            course_id = request.GET.get('course_id', '')
            student = Korisnici.objects.get(id=student_id)
            course = Predmeti.objects.get(id=course_id)
            all_upisi = Upisi.objects.all()
            for upis in all_upisi:
                if upis.student == student and upis.predmet == course:
                    upis_id = upis.id
                    upis = Upisi.objects.get(id=upis_id)
                    upis.status = 'null'
                    upis.save()
                    return redirect('details', student_id)
    else:
        return redirect('login')

def polozen(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            student_id = request.GET.get('student_id', '')
            course_id = request.GET.get('course_id', '')
            student = Korisnici.objects.get(id=student_id)
            course = Predmeti.objects.get(id=course_id)
            all_upisi = Upisi.objects.all()
            for upis in all_upisi:
                if upis.student == student and upis.predmet == course:
                    upis_id = upis.id
            upis = Upisi.objects.get(id=upis_id)
            upis.status = 'passed'
            upis.save()
            return redirect('details', student_id)
    else:
        return redirect('login')

def ponistavanje(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            student_id = request.GET.get('student_id', '')
            course_id = request.GET.get('course_id', '')
            student = Korisnici.objects.get(id=student_id)
            course = Predmeti.objects.get(id=course_id)
            all_upisi = Upisi.objects.all()
            for upis in all_upisi:
                if upis.student == student and upis.predmet == course:
                    upis_id = upis.id
            upis = Upisi.objects.get(id=upis_id)
            upis.status = 'enrolled'
            upis.save()
            return redirect('details', student_id)
    else:
        return redirect('login')

def courses(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            all_courses = Predmeti.objects.all()
            return render(request, 'courses.html', {'all_courses':all_courses})
        else:
            return redirect('index')
    else:
        return redirect('login')

def course_details(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            if request.method == 'GET':
                course_id = request.GET.get('course_id', '')
                course = Predmeti.objects.get(id=course_id)
            return render(request, 'course_details.html', {'course':course})
        else:
            return redirect('index')
    else:
        return redirect('login')

def course_edit(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            course_id = request.GET.get('course_id', '')
            course = Predmeti.objects.get(id=course_id)
            if request.method == 'GET':
                course_form = CourseForm(instance=course)
                return render(request, 'add_course.html', {'form':course_form})
            elif request.method == 'POST':
                course_form = CourseForm(request.POST, instance=course)
                if course_form.is_valid():
                    course_form.save()
                    return redirect('courses')
                else:
                    return HttpResponseNotAllowed()
        else:
            return redirect('index')
    else:
        return redirect('login')

def add_course(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            if request.method == 'GET':
                course_form = CourseForm()
                return render(request, 'add_course.html', {'form':course_form})
            elif request.method == 'POST':
                course_form = CourseForm(request.POST)
                if course_form.is_valid():
                    course_form.save()
                    return redirect('courses')
                else:
                    return HttpResponseNotAllowed()
        else:
            return redirect('index')
    else:
        return redirect('login')

def student_edit(request, student_id):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            student = Korisnici.objects.get(id=student_id)
            if request.method == 'GET':
                student_form = EditStudentForm(instance=student)
                return render(request, 'student_edit.html', {'form':student_form})
            elif request.method == 'POST':
                student_form = EditStudentForm(request.POST, instance=student)
                if student_form.is_valid():
                    student_form.save()
                    return redirect('students')
                else:
                    return HttpResponseNotAllowed()
        else:
            return redirect('index')
    else:
        return redirect('login')

def course_delete(request):
    if request.user.is_authenticated:
        current_user = Korisnici.objects.get(username=request.user)
        if current_user.role == 'mentor':
            course_id = request.GET.get('course_id', '')
            course = Predmeti.objects.get(id=course_id)
            course.delete()
            return redirect('courses')
        else:
            return redirect('index')
    else:
        return redirect('login')