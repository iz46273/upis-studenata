from .models import Korisnici, Upisi, Predmeti
from django import forms
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm


class RegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    class Meta:
        model = Korisnici
        fields = (
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
            'status',
          )

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.role = 'student'
        user.status = self.cleaned_data['status']
        user.email = self.cleaned_data['email']
        usrname = user.email.split('@')
        user.username = usrname[0]
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user

class CourseForm(ModelForm):
    class Meta:
        model = Predmeti
        fields = ['ime', 'kod', 'bodovi', 'sem_redovni', 'sem_izvanredni', 'izborni', 'program']

class EditStudentForm(ModelForm):
    class Meta:
        model = Korisnici
        fields = (
            'first_name',
            'last_name',
            'email',
            'status',
          )
