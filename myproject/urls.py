"""myproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from mentorski import views
from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic import RedirectView

urlpatterns = [
    path ('', views.index),
    path ('accounts/profile/', views.index),
    path('admin/', admin.site.urls),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(template_name='logout.html'), name="logout"),
    path('register/', views.register, name='register'),
    path('index/', views.index, name='index'),
    path('students/', views.students, name='students'),
    path('details/<int:student_id>/', views.details, name='details'),
    path('upis/', views.upis, name='upis'),
    path('ispis/', views.ispis, name='ispis'),
    path('polozen/', views.polozen, name='polozen'),
    path('ponistavanje/', views.ponistavanje, name='ponistavanje'),
    path('courses/', views.courses, name='courses'),
    path('course_details/', views.course_details, name='course_details'),
    path('course_edit/', views.course_edit, name='course_edit'),
    path('add_course/', views.add_course, name='add_course'),
    path('student_edit/<int:student_id>/', views.student_edit, name='student_edit'),
    path('course_delete/', views.course_delete, name='course_delete'),
]